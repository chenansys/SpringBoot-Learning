## 基于Spring-Boot的应用演示

> 样例代码包括以下常用的技术框架(开箱即用)：
- 1.springmvc <br>
- 2.mybatis   <br>
- 3.jpa       <br>
- 4.redis     <br>
- 5.mongo     <br>
- 6.solr      <br>
- 7.dubbo     <br>
- 8.activemq  <br>
- 9.rabbitmq  <br>

<p>
代码均在集成环境下测试通过，如果有测试失败的情况，请QQ:12602575  或者EMail: chenansys#163.com(注意#号) 给我，感谢！
</p>
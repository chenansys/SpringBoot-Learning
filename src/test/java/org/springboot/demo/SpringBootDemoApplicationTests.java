package org.springboot.demo;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springboot.demo.framework.mq.component.AmqpComponent;
import org.springboot.demo.framework.mq.component.JmsComponent;
import org.springboot.demo.service.CityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;


@RunWith(SpringRunner.class)
@SpringBootTest
public class SpringBootDemoApplicationTests {
	@Autowired
	private JmsComponent jmsComponent;
	
	@Autowired
	private AmqpComponent amqpComponent;

	@Autowired
	private CityService cityService;

	/**
	 * activemq测试
	 */
	@Test
	public void jmsSend() {
		String msg = "Hello Spring Boot!";
		System.out.println("Jms发送============"+msg);
		jmsComponent.send(msg);
	}

	/**
	 * rabbitmq测试
	 */
	@Test
	public void amqpSend(){
		String msg = "Hello Spring Boot!";
		System.out.println("amqp发送============"+msg);
		amqpComponent.send(msg);
	}

	/**
	 * redis测试
	 */
	@Test
	public void redisTest(){
		cityService.findCityById(1L);
	}


}

package org.springboot.demo;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;

/**
 * Spring Boot 应用启动类
 */
//Spring Boot 应用的标识
@SpringBootApplication
@EnableCaching
@MapperScan(basePackages = "org.springboot.demo.mapper")
public class SpringBootDemoApplication {

	public static void main(String[] args) {
		// 程序启动入口
        // 启动嵌入式的 Tomcat 并初始化 Spring 环境及其各 Spring 组件
		SpringApplication.run(SpringBootDemoApplication.class, args);
	}
}

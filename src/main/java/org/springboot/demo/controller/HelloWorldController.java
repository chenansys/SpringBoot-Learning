package org.springboot.demo.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

/**
 * 第一个Spring Boot程序 HelloWorld
 */

@Controller
//@RestController
public class HelloWorldController {
	
	@RequestMapping("/hello")
	public ModelAndView sayHello(){
		ModelAndView model = new ModelAndView();
        model.addObject("msg", "Hello Spring Boot !");
		model.setViewName("index");
        return model;
	}
}

package org.springboot.demo.controller;

import org.springboot.demo.domain.City;
import org.springboot.demo.service.CityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import java.util.List;

/**
 * 城市 Controller 实现 Restful HTTP 服务
 */
@Controller
public class CityController {

    @Autowired
    private CityService cityService;

    @RequestMapping(value = "/demo/city/{id}", method = RequestMethod.GET)
    public ModelAndView findOneCity(@PathVariable("id") Long id) {
    	ModelAndView model = new ModelAndView();
        model.addObject("city", cityService.findCityById(id));
        model.setViewName("view");
        return model;
    }

    @RequestMapping(value = "/demo/city", method = RequestMethod.GET)
    public ModelAndView findAllCity() {
    	ModelAndView model = new ModelAndView();
        List<City> cityList = cityService.findAllCity();
        model.addObject("cityList",cityList);
        model.setViewName("view");
        return model;
    }
}

package org.springboot.demo.framework.mq.component;

import javax.jms.Queue;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.jms.core.JmsMessagingTemplate;
import org.springframework.stereotype.Component;

/**
 * activemq的
 */
@Component
public class JmsComponent {

	@Autowired
	private JmsMessagingTemplate jmsMessagingTemplate;
	
	@Autowired
	private Queue queue;

	/**
	 * activemq发送消息
	 */
	//@Scheduled(fixedDelay=3000)//每3s执行1次
	public void send(String msg) {
		this.jmsMessagingTemplate.convertAndSend(this.queue, msg);
	}

	/**
	 * activemq接收者
	 */
	@JmsListener(destination = "${springboot.demo.jms.queue}")
	public void receiveQueue(String text) {
		System.out.println("jms接收============" + text);
	}
}

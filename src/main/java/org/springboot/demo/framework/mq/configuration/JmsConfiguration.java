package org.springboot.demo.framework.mq.configuration;

import javax.jms.Queue;

import org.apache.activemq.command.ActiveMQQueue;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * jms 队列配置
 *
 */
@Configuration
public class JmsConfiguration {

	@Value("${springboot.demo.jms.queue}")
	private String jmsDemoQueue;

	@Bean(name="jmsQueue")
	public Queue queue() {
		return new ActiveMQQueue(jmsDemoQueue);
	}

}
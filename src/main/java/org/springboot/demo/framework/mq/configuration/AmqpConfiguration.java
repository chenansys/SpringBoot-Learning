package org.springboot.demo.framework.mq.configuration;

import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.BindingBuilder;
import org.springframework.amqp.core.DirectExchange;
import org.springframework.amqp.core.Queue;
import org.springframework.amqp.rabbit.annotation.EnableRabbit;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * amqp 队列配置
 */
@EnableRabbit
@Configuration
public class AmqpConfiguration {

	public static final String QUEUENAME = "queue_message_businessMsg_send";
	public static final String EXCHANGE   = "business_Message_Send_Direct";
	public static final String ROUTINGKEY = "message_businessMsg_send";
	/*
	* Exchange: 交换机，决定了消息路由规则；
	* Queue: 消息队列；
	* Channel: 进行消息读写的通道；
	* Bind: 绑定了Queue和Exchange，意即为符合什么样路由规则的消息，将会放置入哪一个消息队列；
	*/
	//DirectExchange
	@Bean
	public DirectExchange defaultExchange() {
		return new DirectExchange(EXCHANGE);
	}


	//Binding，将DirectExchange与Queue进行绑定
	@Bean
	public Binding binding() {
		return BindingBuilder.bind(new Queue(QUEUENAME)).to(new DirectExchange(EXCHANGE)).with(ROUTINGKEY);
	}

}

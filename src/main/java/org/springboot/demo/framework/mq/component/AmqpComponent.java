package org.springboot.demo.framework.mq.component;


import org.springboot.demo.framework.mq.configuration.AmqpConfiguration;
import org.springframework.amqp.core.AmqpTemplate;
import org.springframework.amqp.rabbit.annotation.Exchange;
import org.springframework.amqp.rabbit.annotation.Queue;
import org.springframework.amqp.rabbit.annotation.QueueBinding;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * ActiveMq 接收端
 */
@Component
public class AmqpComponent {

	@Autowired
	private AmqpTemplate amqpTemplate;

	public void send(String msg) {
		//this.amqpTemplate.convertAndSend("queue_message_noticeMsg_send", msg);
		this.amqpTemplate.convertAndSend(AmqpConfiguration.EXCHANGE,AmqpConfiguration.ROUTINGKEY,msg);
	}

	@RabbitListener(bindings = @QueueBinding(value = @Queue(value = "queue_message_businessMsg_send", durable = "true"),
			exchange = @Exchange(value = "business_Message_Send_Direct"),key = "message_businessMsg_send"))
	public void receiveQueue(String text) {
		System.out.println("amqp接收============" + text);
	}

}

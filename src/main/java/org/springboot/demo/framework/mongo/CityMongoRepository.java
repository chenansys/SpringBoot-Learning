package org.springboot.demo.framework.mongo;

import org.springboot.demo.domain.City;

/**
 * Created by Robin on 2017/7/14.
 */
public interface CityMongoRepository {//extends MongoRepository<City, Long> {

    City findByUsername(String username);

}

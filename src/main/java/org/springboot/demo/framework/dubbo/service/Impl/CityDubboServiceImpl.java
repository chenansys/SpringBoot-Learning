package org.springboot.demo.dubbo.service.Impl;

/**
 * Created by Robin on 2017/7/19.
 */

import com.alibaba.dubbo.config.annotation.Service;
import org.springboot.demo.domain.City;
import org.springboot.demo.dubbo.service.CityDubboService;

/**
 * 城市业务 Dubbo 服务层实现层
 *
 * Created by bysocket on 28/02/2017.
 */
// 注册为 Dubbo 服务
@Service(version = "1.0.0")
public class CityDubboServiceImpl implements CityDubboService {

    public City findCityByName(String cityName) {
        return new City(1,1L,"是我的故乡");
    }
}
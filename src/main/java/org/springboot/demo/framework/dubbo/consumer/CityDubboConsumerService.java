package org.springboot.demo.dubbo.consumer;

import com.alibaba.dubbo.config.annotation.Reference;
import org.springboot.demo.domain.City;
import org.springframework.stereotype.Component;

/**
 * Dubbo消费端接口
 * Created by Robin on 2017/7/19.
 */
@Component
public interface CityDubboConsumerService {

    /* 消费端放开
    @Reference(version = "1.0.0")
    CityDubboService cityDubboService;

    public void printCity() {
        String cityName="温岭";
        City city = cityDubboService.findCityByName(cityName);
        System.out.println(city.toString());
    }*/
}

package org.springboot.demo.framework.solr;

/**
 * Solr服务器端配置
 * Created by Robin on 2017/6/22.
 */

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.solr.repository.config.EnableSolrRepositories;

@Configuration
@EnableSolrRepositories(multicoreSupport = true)
@ConfigurationProperties(prefix="spring.solr")
public class SolrConfig {

    private static final Logger logger = LoggerFactory.getLogger(SolrConfig.class);

    //===========单机模式==========
    /*@Value("${spring.data.solr.host}")
    private String solrHost;

    @Bean
    public SolrClient solrClient() {
        logger.info("配置SolrClient");
        //return new HttpSolrClient(environment.getRequiredProperty("spring.data.solr.host"));
        return new HttpSolrClient(solrHost);
    }*/

    //===========集群模式==========
    @Value("${spring.data.solr.zkHost}")
    private String zkHost;

    @Value("${spring.data.solr.defaultCollection}")
    private String defaultCollection;


    public String getZkHost() {
        return zkHost;
    }

    public void setZkHost(String zkHost) {
        this.zkHost = zkHost;
    }

    public String getDefaultCollection() {
        return defaultCollection;
    }

    public void setDefaultCollection(String defaultCollection) {
        this.defaultCollection = defaultCollection;
    }

}

package org.springboot.demo.framework.solr;

import org.apache.solr.client.solrj.impl.CloudSolrServer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.util.StringUtils;

import javax.annotation.PreDestroy;
import java.io.IOException;

/**
 * Solr客户端配置
 * Created by Robin on 2018/2/26.
 */

@Configuration
@EnableConfigurationProperties(SolrConfig.class)
public class SolrClientConfig {

    @Autowired
    private SolrConfig solrConfig;

    private CloudSolrServer solrServer;

    @PreDestroy
    public void close() {
        if (this.solrServer != null) {
            try {
                this.solrServer.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    @Bean
    public CloudSolrServer SolrServer(){
        if(StringUtils.hasText(this.solrConfig.getZkHost())){
            solrServer = new CloudSolrServer(this.solrConfig.getZkHost());
            solrServer.setDefaultCollection(this.solrConfig.getDefaultCollection());
        }
        return this.solrServer;
    }
}

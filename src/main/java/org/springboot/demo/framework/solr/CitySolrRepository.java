package org.springboot.demo.framework.solr;

import org.springboot.demo.domain.Borrow;
import org.springframework.data.solr.repository.SolrCrudRepository;

/**
 * Solr接口调用
 * Created by Chenan on 2017/7/19.
 */
public interface CitySolrRepository {//extends SolrCrudRepository<Borrow, String> {


    Iterable<Borrow> findByProName(String name);
}

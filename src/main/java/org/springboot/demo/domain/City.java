package org.springboot.demo.domain;

import java.io.Serializable;
import org.apache.commons.lang3.builder.ReflectionToStringBuilder;
import org.apache.solr.client.solrj.beans.Field;
import org.springframework.data.annotation.Id;
import org.springframework.data.solr.core.mapping.SolrDocument;

/**
 * 实体类
 */
@SolrDocument(solrCoreName="City") // Solr collection name
public class City implements Serializable{
	
	 private static final long serialVersionUID = -1L;

    /**
     * 城市编号
     */
    @Field("id")
    @Id
    private Integer id;

    /**
     * 省份编号
     */
    @Field
    private Long provinceId;

    /**
     * 城市名称
     */
    @Field
    private String cityName;

    @Field
    private String proName;

    /**
     * 描述
     */
    @Field
    private String description;

    public City(){}
    public City(Integer id, Long provinceId, String cityName) {
        this.id = id;
        this.provinceId = provinceId;
        this.cityName = cityName;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Long getProvinceId() {
        return provinceId;
    }

    public void setProvinceId(Long provinceId) {
        this.provinceId = provinceId;
    }

    public String getCityName() {
        return cityName;
    }

    public void setCityName(String cityName) {
        this.cityName = cityName;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }


    public String toString() {
        return ReflectionToStringBuilder.toString(this);
    }


}

package org.springboot.demo.domain;

import java.util.Date;

import javax.validation.constraints.Max;

import org.apache.commons.lang3.builder.ReflectionToStringBuilder;
import org.apache.solr.client.solrj.beans.Field;
import org.hibernate.validator.constraints.Length;
import org.springframework.data.solr.core.mapping.SolrDocument;

/**
 *Description: 产品信息实体类
 *Class_Name: Borrow
 *@Author: Chenan
 *Date: 2016/5/31 11:29
 *@Version: 1.0
 */

@SolrDocument(solrCoreName="debtbuy") // Solr collection name
public class Borrow implements java.io.Serializable{
	private static final long serialVersionUID = 1L;

	/**
	 * 主键id
	 */
	@Max(9999999999L)
	@Field("id")
	private Integer id;
	/**
	 * 产品ID
	 */
	@Max(9999999999L)
	@Field("proId")
	private Integer proId;
	/**
	 * 产品加密后ID
	 */
	@Max(9999999999L)
	@Field("proIds")
	private String proIds;


	/**
	 * 产品名称
	 */
	@Length(max=100)
	@Field("proName")
	private String proName;
	/**
	 * 标的状态（0尚未发布 1.已发布，认购中  2认购完成，审核中  3计息中 4到期完成 5强制结束）
	 */
	@Max(9999999999L)
	@Field("proStatus")
	private Integer proStatus;
	/**
	 * 标ID
	 */
	@Max(9999999999L)
	@Field("borrowId")
	private Integer borrowId;
	/**
	 * borrowId加密后
	 */
	@Max(9999999999L)
	@Field("borrowIds")
	private String borrowIds;

	/**
	 * 标编号
	 */
	@Length(max=32)
	@Field("borrowNo")
	private String borrowNo;
	/**
	 * 借款人id
	 */
	@Max(9999999999L)
	@Field("loanUserId")
	private Integer loanUserId;

	/**
	 * 借款人id,加密
	 */
	private String loanUserIds;
	/**
	 * 借款人编号
	 */
	@Length(max=32)
	@Field("loanUserNo")
	private String loanUserNo;
	/**
	 * 借款人姓名
	 */
	@Length(max=11)
	@Field("loanUserName")
	private String loanUserName;
	/**
	 * 借款人头像
	 */
	@Length(max=300)
	@Field("loanHeadImgUrl")
	private String loanHeadImgUrl;
	/**
	 * 借款标题
	 */
	@Length(max=255)
	@Field("loanTitle")
	private String loanTitle;
	/**
	 * 借款总金额
	 */
	@Max(9223372036854775807L)
	@Field("loanAmount")
	private Double loanAmount;
	/**
	 * 已融资额度
	 */
	@Max(9223372036854775807L)
	@Field("loanTenderSum")
	private Double loanTenderSum;
	/**
	 * 还款方式 1等额本金 2等额本息 3还本付息 4等额本息 5还本付息
	 */
	@Max(9999999999L)
	@Field("loanRepaymentWay")
	private Integer loanRepaymentWay;
	/**
	 * 借款期限（单位：月）
	 */
	@Max(9999999999L)
	@Field("loanTimeLimit")
	private Integer loanTimeLimit;
	/**
	 * 借款利率
	 */
	@Max(999L)
	@Field("loanRateReal")
	private Double loanRateReal;

	/**
	 * 产品副利率
	 */
	@Max(999L)
	@Field("assistantInterestRate")
	private Double assistantInterestRate;

	/**
	 * 借款类型(房抵标、翼龙贷、产业链 等等)
	 */
	@Max(9999999999L)
	@Field("loanType")
	private Integer loanType;
	/**
	 * 借款人信用评级
	 */
	@Max(9999999999L)
	@Field("loanCreditLevel")
	private Integer loanCreditLevel;

	/**
	 * 借款人所在省
	 */
	@Length(max=11)
	@Field("loanUserProvince")
	private String loanUserProvince;

	/**
	 * 借款人所在城市
	 */
	@Length(max=11)
	@Field("loanUserCity")
	private String loanUserCity;

	/**
	 * 借款人所在县
	 */
	@Length(max=11)
	@Field("loanUserCounty")
	private String loanUserCounty;

	/**
	 * 借款用途
	 */
	@Max(9999999999L)
	@Field("loanPurpose")
	private Integer loanPurpose;
	/**
	 * 借款人性别
	 */
	@Length(max=1)
	@Field("loanSex")
	private String loanSex;


	/**
	 * 借款人婚姻状况
	 */
	@Length(max=50)
	@Field("loanMarriageStatus")
	private String loanMarriageStatus;
	/**
	 * 借款人职业
	 */
	@Max(9999999999L)
	@Field("loanProfession")
	private Integer loanProfession;
	/**
	 * 借款人教育水平
	 */
	@Max(9999999999L)
	@Field("loanEducationalStatus")
	private Integer loanEducationalStatus;
	/**
	 * 借款人年龄范围
	 */
	@Length(max=50)
	@Field("loanAge")
	private String loanAge;
	/**
	 * 借款人年收入
	 */
	@Length(max=50)
	@Field("loanIncome")
	private String loanIncome;
	/**
	 * 贷款记录
	 */
	@Length(max=50)
	@Field("loanRecords")
	private String loanRecords;

	/**
	 * 投标期限（单位：天）
	 */
	@Max(9999999999L)
	@Field("borrowActiveDay")
	private Integer borrowActiveDay;
	/**
	 * 投标期限（单位：小时）
	 */
	@Max(9999999999L)
	@Field("borrowActiveHour")
	private Integer borrowActiveHour;
	/**
	 * 发布时间
	 */
	@Max(9999999999L)
	@Field("borrowPublishTime")
	private Date borrowPublishTime;
	/**
	 * 创建时间
	 */
	@Max(9999999999L)
	@Field("createTime")
	private Date createTime;

	private Date updateTime;

	private Date proOverTime;
	/**
	 * 标结束时间（流标时间、还款完成时间）
	 */
	@Max(9999999999L)
	@Field("borrowOverTime")
	private Date borrowOverTime;

	@Max(9999999999L)
	@Field("surplusTenderAmt")
	private Double surplusTenderAmt;//剩余可投金额

	/**
	 * 完成进度= 已融资额度/借款总金额
	 */
	@Field("completionProgress")
	private Double completionProgress;
	/**
	 * 债券所属行业
	 */
	@Length(max=50)
	@Field("borrowIndustry")
	private String borrowIndustry;
	/**
	 * 用户类型，0、个人；1、企业
	 */
	@Max(9999999999L)
	@Field("loanUserType")
	private Integer loanUserType;
	/**
	 * 借款人姓名明文（不做处理）
	 */
	private String borrowUserName;
	/**
	 * 借款人 身份证号
	 */
	private String loanIdcard; //身份证号
	private String loanUserNameStr;  //处理后的用户名
	private String loanTitleStr;  //标题处理
	private String loanAmountStr;  //金额处理
	private String loanRateRealStr; //利率处理
	private String assistantInterestRateStr; //副利率处理
	private String loanRepaymentWayStr;//还款方式字符串
	private String loanIdcardStr;  //身份证处理
	private String overProgress;//完成进度

	public String getBorrowIndustry() {
		return borrowIndustry;
	}
	public void setBorrowIndustry(String borrowIndustry) {
		this.borrowIndustry = borrowIndustry;
	}
	public Integer getLoanUserType() {
		return loanUserType;
	}
	public void setLoanUserType(Integer loanUserType) {
		this.loanUserType = loanUserType;
	}
	public String getBorrowUserName() {
		return borrowUserName;
	}
	public void setBorrowUserName(String borrowUserName) {
		this.borrowUserName = borrowUserName;
	}
	public String getOverProgress() {
		return overProgress;
	}

	public void setOverProgress(String overProgress) {
		this.overProgress = overProgress;
	}

	public String getLoanIdcard() {
		return loanIdcard;
	}

	public void setLoanIdcard(String loanIdcard) {
		this.loanIdcard = loanIdcard;
	}

	public String getLoanIdcardStr() {

		return loanIdcardStr;
	}

	public void setLoanIdcardStr(String loanIdcardStr) {
		this.loanIdcardStr = loanIdcardStr;
	}

	public String getLoanAmountStr() {
		return "0.00";
	}

	public void setLoanAmountStr(String loanAmountStr) {
		this.loanAmountStr = loanAmountStr;
	}

	public String getLoanRateRealStr() {

			return "0.00";

	}

	public void setLoanRateRealStr(String loanRateRealStr) {
		this.loanRateRealStr = loanRateRealStr;
	}

	public String getAssistantInterestRateStr() {

			return "0.00";

	}

	public void setAssistantInterestRateStr(String assistantInterestRateStr) {
		this.assistantInterestRateStr = assistantInterestRateStr;
	}

	public String getLoanTitleStr() {

		return loanTitle;
	}
	/*
	 * 给投资列表的solr标题使用
	 */
	public String getLoanTitleStr2() {

		return loanTitle;
	}
	public void setLoanTitleStr(String loanTitleStr) {
		this.loanTitleStr = loanTitleStr;
	}

	public String getLoanUserNameStr() {

			return loanUserName;

	}

	public void setLoanUserNameStr(String loanUserNameStr) {
		this.loanUserNameStr = loanUserNameStr;
	}

	public Double getCompletionProgress() {
		return completionProgress;
	}

	public void setCompletionProgress(Double completionProgress) {
		this.completionProgress = completionProgress;
	}

	public Double getSurplusTenderAmt() {
		return surplusTenderAmt;
	}

	public void setSurplusTenderAmt(Double surplusTenderAmt) {
		this.surplusTenderAmt = surplusTenderAmt;
	}

	public Borrow(){
	}

	public Borrow(Integer id){
		this.id = id;
	}
	/**
	 * 表的uuid
	 */
	private Double tableUuid;

	public Double getTableUuid() {
		return tableUuid;
	}

	public void setTableUuid(Double tableUuid) {
		this.tableUuid = tableUuid;
	}
	public void setId(Integer value) {
		this.id = value;
	}

	public Integer getId() {
		return this.id;
	}
	public void setProId(Integer value) {
		this.proId = value;
	}

	public Integer getProId() {
		return this.proId;
	}
	public void setProName(String value) {
		this.proName = value;
	}

	public String getProName() {
		return this.proName;
	}
	public void setProStatus(Integer value) {
		this.proStatus = value;
	}

	public Integer getProStatus() {
		return this.proStatus;
	}
	public void setBorrowId(Integer value) {
		this.borrowId = value;
	}

	public Integer getBorrowId() {
		return this.borrowId;
	}
	public void setBorrowNo(String value) {
		this.borrowNo = value;
	}

	public String getBorrowNo() {
		return this.borrowNo;
	}
	public void setLoanUserId(Integer value) {
		this.loanUserId = value;
	}

	public Integer getLoanUserId() {
		return this.loanUserId;
	}

	public String getLoanUserIds() {
		return loanUserIds;
	}

	public void setLoanUserIds(String loanUserIds) {
		this.loanUserIds = loanUserIds;
	}

	public void setLoanUserNo(String value) {
		this.loanUserNo = value;
	}

	public String getLoanUserNo() {
		return this.loanUserNo;
	}
	public void setLoanUserName(String value) {
		this.loanUserName = value;
	}

	public String getLoanUserName() {
		return this.loanUserName;
	}
	public void setLoanUserCity(String value) {
		this.loanUserCity = value;
	}

	public String getLoanUserCity() {
		return this.loanUserCity;
	}

	public void setLoanTitle(String value) {
		this.loanTitle = value;
	}

	public String getLoanTitle() {
		return this.loanTitle;
	}
	public void setLoanAmount(Double value) {
		if(null==value){
			this.loanAmount = new Double("0.00");
		}else{
			this.loanAmount = value;
		}
	}

	public Double getLoanAmount() {
		return this.loanAmount;
	}
	public void setLoanTenderSum(Double value) {
		if(null==value){
			this.loanTenderSum = new Double("0.00");
		}else{
			this.loanTenderSum = value;
		}

	}

	public Double getLoanTenderSum() {
		return this.loanTenderSum;
	}
	public void setLoanTimeLimit(Integer value) {
		this.loanTimeLimit = value;
	}

	public Integer getLoanTimeLimit() {
		return this.loanTimeLimit;
	}
	public void setLoanRateReal(Double value) {
		this.loanRateReal = value;
	}

	public Double getLoanRateReal() {
		return this.loanRateReal;
	}

	public void setLoanRepaymentWay(Integer value) {
		this.loanRepaymentWay = value;
	}

	public Integer getLoanRepaymentWay() {
		return this.loanRepaymentWay;
	}
	public void setLoanType(Integer value) {
		this.loanType = value;
	}

	public Integer getLoanType() {
		return this.loanType;
	}
	public void setLoanPurpose(Integer value) {
		this.loanPurpose = value;
	}

	public Integer getLoanPurpose() {
		return this.loanPurpose;
	}
	public void setBorrowActiveDay(Integer value) {
		this.borrowActiveDay = value;
	}

	public Integer getBorrowActiveDay() {
		return this.borrowActiveDay;
	}
	public void setBorrowActiveHour(Integer value) {
		this.borrowActiveHour = value;
	}

	public Integer getBorrowActiveHour() {
		return this.borrowActiveHour;
	}
	public void setBorrowPublishTime(Date value) {
		this.borrowPublishTime = value;
	}

	public Date getBorrowPublishTime() {
		return this.borrowPublishTime;
	}

	public void setCreateTime(Date value) {
		this.createTime = value;
	}

	public Date getCreateTime() {
		return this.createTime;
	}
	public void setBorrowOverTime(Date value) {
		this.borrowOverTime = value;
	}

	public Date getBorrowOverTime() {
		return this.borrowOverTime;
	}

	public Date getProOverTime() {
		return proOverTime;
	}

	public void setProOverTime(Date proOverTime) {
		this.proOverTime = proOverTime;
	}

	public String getLoanHeadImgUrl() {
		return loanHeadImgUrl;
	}

	public void setLoanHeadImgUrl(String loanHeadImgUrl) {
		this.loanHeadImgUrl = loanHeadImgUrl;
	}

	public Integer getLoanCreditLevel() {
		return loanCreditLevel;
	}

	public void setLoanCreditLevel(Integer loanCreditLevel) {
		this.loanCreditLevel = loanCreditLevel;
	}

	public String getLoanSex() {
		return loanSex;
	}

	public void setLoanSex(String loanSex) {
		this.loanSex = loanSex;
	}

	public String getLoanMarriageStatus() {
		return loanMarriageStatus;
	}

	public void setLoanMarriageStatus(String loanMarriageStatus) {
		this.loanMarriageStatus = loanMarriageStatus;
	}

	public Integer getLoanProfession() {
		return loanProfession;
	}

	public void setLoanProfession(Integer loanProfession) {
		this.loanProfession = loanProfession;
	}

	public Integer getLoanEducationalStatus() {
		return loanEducationalStatus;
	}

	public void setLoanEducationalStatus(Integer loanEducationalStatus) {
		this.loanEducationalStatus = loanEducationalStatus;
	}

	public String getLoanAge() {
		return loanAge;
	}

	public void setLoanAge(String loanAge) {
		this.loanAge = loanAge;
	}

	public String getLoanIncome() {
		return loanIncome;
	}

	public void setLoanIncome(String loanIncome) {
		this.loanIncome = loanIncome;
	}

	public String getLoanRecords() {
		return loanRecords;
	}

	public void setLoanRecords(String loanRecords) {
		this.loanRecords = loanRecords;
	}

	public String getLoanUserProvince() {
		return loanUserProvince;
	}

	public void setLoanUserProvince(String loanUserProvince) {
		this.loanUserProvince = loanUserProvince;
	}

	public String getLoanUserCounty() {
		return loanUserCounty;
	}

	public void setLoanUserCounty(String loanUserCounty) {
		this.loanUserCounty = loanUserCounty;
	}

	public String getProIds() {
		return proIds;
	}

	public void setProIds(String proIds) {
		this.proIds = proIds;
	}

	public String getBorrowIds() {
		return borrowIds;
	}

	public void setBorrowIds(String borrowIds) {
		this.borrowIds = borrowIds;
	}

	public Date getUpdateTime() {
		return updateTime;
	}

	public void setUpdateTime(Date updateTime) {
		this.updateTime = updateTime;
	}

	public Double getAssistantInterestRate() {
		return 0.0;
	}

	public void setAssistantInterestRate(Double value) {
		this.assistantInterestRate = value;
	}


	public String toString() {
		return ReflectionToStringBuilder.toString(this);
	}

}


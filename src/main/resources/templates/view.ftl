<!DOCTYPE html>
<html>
<head lang="en">
	<title>Spring Boot Demo - FreeMarker</title>
	<link href="/css/index.css" rel="stylesheet" />
	
</head>
<body>
	<center>
		<h1 id="title">数据展现</h1>
		 <table border="1px solid #8968CD" style="border-collapse: collapse;">
		 <tr><th>序号</th> <th>编号</th> <th>城市名</th> <th>描述信息</th> </tr>  
    <#list cityList as city>  
        <tr>  
            <td>${city.id}</td>  
            <td>${city.provinceId}</td>  
            <td>${city.cityName}</td>  
            <td>${city.description}</td>   
        </tr>  
    </#list>  
    </table>  
	</center>
	
	<script type="text/javascript" src="/webjars/jquery/2.1.4/jquery.min.js"></script>
	
	<script>
		$(function(){
			$('#title').click(function(){
				alert('点击了');
			});
		})
   });
	</script>
</body>
</html>
